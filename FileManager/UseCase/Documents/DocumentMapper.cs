﻿using AutoMapper;

namespace FileManager
{
    public class DocumentMapper: Profile
    {
        public DocumentMapper() {

            CreateMap<SaveDocumentDto, Document>();
            CreateMap<Document, DocumentDto>();
        }
    }
}