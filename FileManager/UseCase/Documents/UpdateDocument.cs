﻿using AutoMapper;
using FileManager.Data;
using Microsoft.EntityFrameworkCore;

namespace FileManager
{
    public interface IUpdateDocument
    { 
        public Task<DocumentDto> Update(SaveDocumentDto request);
    }

    public class UpdateDocument : IUpdateDocument
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public UpdateDocument(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<DocumentDto> Update(SaveDocumentDto request)
        {
            var query = await _context.Documents
                .FirstOrDefaultAsync(x => x.Id == request.Id);

            if (query != default)
            {
                query.Content = request.Content;
                query.FileName = request.FileName;
                query.Type = request.Type;

                _context.Update(query);
                _context.SaveChanges();
            }
            return _mapper.Map<DocumentDto>(query);
        }
    }
}
