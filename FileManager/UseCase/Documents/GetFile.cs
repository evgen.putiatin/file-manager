﻿using FileManager.Data;
using Microsoft.EntityFrameworkCore;

namespace FileManager
{
    public interface IGetFile
    {
        public Task<byte[]> Get(string documentId);
    }

    public class GetFile : IGetFile
    {
        private readonly ApplicationDbContext _context;

        public GetFile(ApplicationDbContext context)
        {
            _context= context;
        }

        public async Task<byte[]> Get(string documentId)
        {
            var query = await _context.Documents
                .Where(d => d.Id == documentId)
                .FirstOrDefaultAsync();

            return query != default ? query.Content : new byte[0];
        }
    }
}
