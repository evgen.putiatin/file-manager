﻿using FileManager.Data;
using Microsoft.EntityFrameworkCore;

namespace FileManager
{
    public interface IDeleteDocument
    {
        public Task DeleteAsync(string documentId);
    }

    public class DeleteDocument : IDeleteDocument
    {
        private readonly ApplicationDbContext _context;

        public DeleteDocument(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task DeleteAsync(string documentId)
        {
            var query = await _context.Documents
                .FirstOrDefaultAsync(x => x.Id == documentId);

            if (query != null)
            {
                _context.Remove(query);
                await _context.SaveChangesAsync();

            }

        }
    }
}
