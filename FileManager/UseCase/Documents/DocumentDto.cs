﻿namespace FileManager
{
    public class DocumentDto
    {
        public string Id { get; set; }
        public string FileName { get; set; }
        public byte[] Content { get; set; }
        public string Type { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}