﻿namespace FileManager
{
    public class SaveDocumentDto
    {
        public string Id { get; set; }
        public string FileName { get; set; }
        public byte[] Content { get; set; }
        public string Type { get; set; }
    }
}
