﻿using AutoMapper;
using FileManager.Data;
using FileManager.Services;

namespace FileManager
{
    public interface ICreateDocument
    {
        public Task<DocumentDto> Create(SaveDocumentDto request);
    }

    public class CreateDocument : ICreateDocument
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public CreateDocument(ApplicationDbContext context, IMapper mapper, IUserService userService)
        {
            _context = context;
            _mapper = mapper;
            _userService = userService;
        }

        public async Task<DocumentDto> Create(SaveDocumentDto request)
        {
            var saveFile = _mapper.Map<Document>(request);
            saveFile.Id = Guid.NewGuid().ToString();
            saveFile.AddedBy = (await _userService.GetCurrentUser()).Id; ;
            await _context.AddAsync(saveFile);
            _context.SaveChanges();

            return _mapper.Map<DocumentDto>(saveFile);
        }
    }
}
