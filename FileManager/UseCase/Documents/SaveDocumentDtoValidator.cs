﻿using FluentValidation;

namespace FileManager
{
    public class SaveDocumentDtoValidator: AbstractValidator<SaveDocumentDto>
    {
        public SaveDocumentDtoValidator()
        {
            RuleFor(x => x.FileName)
                .NotEmpty();
            RuleFor(x => x.Content)    
                .NotEmpty();
            RuleFor(x => x.Type)
                .NotEmpty();
        }
    }
}
