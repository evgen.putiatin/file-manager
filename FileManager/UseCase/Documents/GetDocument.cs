﻿using AutoMapper;
using FileManager.Data;
using FileManager.Services;
using Microsoft.EntityFrameworkCore;

namespace FileManager
{
    public interface IGetDocument {

        public Task<DocumentDto> Get(string documentId);
        public Task<List<DocumentDto>> GetList();
    }

    public class GetDocument : IGetDocument
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public GetDocument(ApplicationDbContext context, IMapper mapper, IUserService userService)
        {
            _context = context;
            _mapper = mapper;
            _userService = userService;
        }

        public async Task<DocumentDto> Get(string documentId)
        {
            var query = await _context.Documents
                .Where(x => x.Id == documentId)
                .FirstOrDefaultAsync();

            return _mapper.Map<DocumentDto>(query);
        }

        public async Task<List<DocumentDto>> GetList()
        {
            var currentUser = (await _userService.GetCurrentUser()).Id;

            var query = await _context.Documents
                .Where(x => x.AddedBy == currentUser)
                .ToListAsync();

            return _mapper.Map<List<DocumentDto>>(query);
        }
    }
}
