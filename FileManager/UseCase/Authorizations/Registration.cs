﻿

using FileManager.Data;
using FileManager.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FileManager
{
    public interface IRegistration
    {
        public Task<AuthorizationDto> Execute(RegistrationDto request);
    }

    public class Registration : IRegistration
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IdentityServices _identityServices;

        public Registration(ApplicationDbContext dbContext,
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IdentityServices identityServices)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _signInManager = signInManager;
            _identityServices = identityServices;
        }

        public async Task<AuthorizationDto> Execute(RegistrationDto request)
        {
            AuthorizationDto response = new AuthorizationDto();
            IdentityUser user = _dbContext.Users.FirstOrDefault(e => e.Email == request.Email);
            if (user == default)
            {
                IdentityUser newUser = new IdentityUser()
                {
                    Email = request.Email,
                    UserName = request.Email
                };

                var createUser = await _userManager.CreateAsync(newUser, request.Password);

                await _signInManager.SignInAsync(newUser, false);
                if (!createUser.Succeeded)
                {
                    response.ErrorMessage = "Invalid username or password.";
                    return response;
                }
                else
                {
                    return await _identityServices.GetIdentity(newUser, request.Password, response);
                }
            }
            else
            {
                response.ErrorMessage = "Incorrect email";
                return response;
            }
        }
    }
}
