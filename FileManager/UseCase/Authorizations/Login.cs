﻿using FileManager.Data;
using Microsoft.AspNetCore.Identity;
using FileManager.Services;

namespace FileManager
{
    public interface ILogin
    {
        public Task<AuthorizationDto> Execute(LoginDto request);
    }

    public class Login : ILogin
    {
        private ApplicationDbContext _dbContext;
        private SignInManager<IdentityUser> _signInManager;
        private IdentityServices _identityServices;

        public Login(ApplicationDbContext dbContext,
            SignInManager<IdentityUser> signInManager,
            IdentityServices identityServices)
        {
            _dbContext = dbContext;
            _signInManager = signInManager;
            _identityServices = identityServices;
        }

        public async Task<AuthorizationDto> Execute(LoginDto request)
        {

            AuthorizationDto response = new AuthorizationDto();
            IdentityUser user = _dbContext.Users.FirstOrDefault(e => e.Email == request.Email);
            if (user != default)
            {
                var result = await _signInManager.PasswordSignInAsync(user, request.Password, true, true);
                if (result.IsLockedOut)
                {
                    response.ErrorMessage = "Your account locked out from 5 min.";
                    return response;
                }
                else if (!result.Succeeded)
                {
                    response.ErrorMessage = "Invalid username or password.";
                    return response;
                }
                else
                {
                    return await _identityServices.GetIdentity(user, request.Password, response);
                }
            }
            else
            {
                response.ErrorMessage = "Incorrect email";
                return response;
            }
        }
    }
}
