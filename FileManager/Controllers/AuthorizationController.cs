﻿using Microsoft.AspNetCore.Mvc;

namespace FileManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private readonly IRegistration _registration;
        private readonly ILogin _login;

        public AuthorizationController(IRegistration registration, ILogin login)
        {
            _registration = registration;
            _login = login;
        }

        [HttpPost]
        [Route("registration")]
        public async Task<AuthorizationDto> Registration([FromBody] RegistrationDto request)
        {
            return await _registration.Execute(request);
        }

        [HttpPost]
        [Route("login")]
        public async Task<AuthorizationDto> Login([FromBody] LoginDto request)
        {
            return await _login.Execute(request);
        }
    }
}
