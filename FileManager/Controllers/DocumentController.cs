﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FileManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DocumentController : ControllerBase
    {
        private readonly IGetDocument _getDocument;
        private readonly IGetFile _getFile;
        private readonly ICreateDocument _createDocument;
        private readonly IUpdateDocument _updateDocument;
        private readonly IDeleteDocument _deleteDocument;

        public DocumentController(IGetDocument getDocument,
            IGetFile getFile,
            ICreateDocument createDocument,
            IUpdateDocument updateDocument,
            IDeleteDocument deleteDocument)
        {
            _getDocument = getDocument;
            _getFile = getFile;
            _createDocument = createDocument;
            _updateDocument = updateDocument;
            _deleteDocument = deleteDocument;
        }

        [HttpGet]
        [Route("{documentId}")]
        public async Task<DocumentDto> Get(string documentId)
        {
            return await _getDocument.Get(documentId);
        }

        [HttpGet]
        [Route("list")]
        public async Task<List<DocumentDto>> List()
        {
            return await _getDocument.GetList();
        }

        [HttpGet]
        [Route("file/{documentId}")]
        public async Task<byte[]> GetFile(string documentId)
        {
            return await _getFile.Get(documentId);
        }

        [HttpPost]
        [Route("create")]
        public async Task<DocumentDto> Create([FromBody] SaveDocumentDto request)
        {
            return await _createDocument.Create(request);
        }

        [HttpPost]
        [Route("update")]
        public async Task<DocumentDto> Update([FromBody] SaveDocumentDto request)
        {
            return await _updateDocument.Update(request);
        }

        [HttpDelete]
        [Route("delete/{documentId}")]
        public async Task Delete(string documentId)
        {
            await _deleteDocument.DeleteAsync(documentId);
        }
    }
}
