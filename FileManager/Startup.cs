﻿using FileManager.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using FileManager.Services;
using System.Reflection;
using Microsoft.Extensions.Options;

namespace FileManager
{
    public class Startup
    {
        public IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(
                _configuration.GetConnectionString("DefaultConnection"));
                options.EnableSensitiveDataLogging(true);
            });

            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddAuthentication(cfg =>
            {
                cfg.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                cfg.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                cfg.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                            .AddJwtBearer(option =>
                            {
                                option.RequireHttpsMetadata = false;
                                option.SaveToken = true;
                                option.TokenValidationParameters = new TokenValidationParameters
                                {
                                    ValidateIssuer = true,
                                    ValidIssuer = AuthOptions.ISSUER,
                                    ValidateAudience = true,
                                    ValidAudience = AuthOptions.AUDIENCE,
                                    ValidateLifetime = true,
                                    IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                                    ValidateIssuerSigningKey = true
                                };
                            });

            services.AddScoped<IdentityServices>();
            services.AddTransient<IRegistration, Registration>();
            services.AddTransient<ILogin, Login>();
            services.AddAutoMapper(typeof(DocumentMapper));
            services.AddTransient<ICreateDocument, CreateDocument>();
            services.AddTransient<IGetDocument, GetDocument>();
            services.AddTransient<IGetFile, GetFile>();
            services.AddTransient<IUpdateDocument, UpdateDocument>();
            services.AddTransient<IDeleteDocument, DeleteDocument>();
            services.AddTransient<IUserService, UserService>();
            services.AddControllersWithViews();
            services.AddHttpContextAccessor();
            services.AddConnections();
            services.AddRazorPages();
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("FileManagerApi", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "FileManager API"
                });
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter bearer token",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT",
                    Scheme = "bearer"
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type=ReferenceType.SecurityScheme,
                                Id="Bearer"
                            }
                        },
                        new List<string>()
                    }
                });

                var fileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var filePath = Path.Combine(System.AppContext.BaseDirectory, fileName);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwagger(options =>
            {
                options.SerializeAsV2 = true;
            });
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/FileManagerApi/swagger.json", "FileManagerApi");
                options.RoutePrefix = string.Empty;
            });
        }
    }
}
