﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FileManager
{
    public class DocumentConfiguration : IEntityTypeConfiguration<Document>
    {
        public void Configure(EntityTypeBuilder<Document> builder)
        {
            builder
                .Property(x => x.Id)
                .HasDefaultValueSql("newid()"); ;

            builder
               .Property(x => x.FileName)
               .HasMaxLength(64);

            builder
                .Property(x => x.Type)
                .HasMaxLength(4);
        }
    }
}
