﻿using FileManager;
using FileManager.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace FileManager.Services
{
    public class IdentityServices
    {
        private UserManager<IdentityUser> _userManager;
        public IdentityServices(UserManager<IdentityUser> context)
        {
            _userManager = context;
        }
        public async Task<AuthorizationDto> GetIdentity(IdentityUser user, string password, AuthorizationDto response)
        {
            ClaimsIdentity claimsIdentity = default;
            if (user != null && await _userManager.CheckPasswordAsync(user, password))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                };
                
                claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
        
                response.Id = user.Id;
                response.UserName = user.UserName;
                response.Email = user.Email;
                response.Token = GetJwtSecurityToken(claimsIdentity);
                response.ExpireDate = DateTime.Now.AddDays(AuthOptions.LifeDay);
                response.Successful= true;
                return response;
            }
            else
            {
                response.ErrorMessage = "Invalid username or password.";
                return response;
            }
        }
        private string GetJwtSecurityToken(ClaimsIdentity identity)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.AddDays(AuthOptions.LifeDay),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }
    }
}
