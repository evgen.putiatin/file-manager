﻿using Microsoft.AspNetCore.Identity;

namespace FileManager.Services
{
    public interface IUserService
    {
        public Task<IdentityUser> GetCurrentUser();
    }

    public class UserService : IUserService
    {

        private readonly IHttpContextAccessor _contextAccessor;
        private readonly UserManager<IdentityUser> _userManager;

        public UserService(IHttpContextAccessor contextAccessor,
            UserManager<IdentityUser> userManager)
        {

            _contextAccessor = contextAccessor;
            _userManager = userManager;
        }

        public async Task<IdentityUser> GetCurrentUser()
        {
            return await _userManager.FindByNameAsync(_contextAccessor.HttpContext.User.Identity.Name);
        }
    }
}
