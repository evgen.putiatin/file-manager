﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FileManager.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Document> Documents { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new DocumentConfiguration());
        }

        public override int SaveChanges()
        {
            UpdateDate();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            UpdateDate();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void UpdateDate()
        {
            var entries = ChangeTracker
                .Entries()
                    .Where(e => e.Entity is IEntity && (
                    e.State == EntityState.Added ||
                    e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((IEntity)entityEntry.Entity).ModifiedDate = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                    ((IEntity)entityEntry.Entity).CreatedDate = DateTime.Now;
            }
        }
    }
}